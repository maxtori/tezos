// SPDX-FileCopyrightText: 2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

// Named register mapping, named after RISC-V Spec ABI
// E.g. a7 => x17
pub const A0: u64 = 10;
pub const A1: u64 = 11;
pub const A6: u64 = 16;
pub const A7: u64 = 17;
